import unittest
import database_generator
import sqlite3

class DatabaseSelectieTests(unittest.TestCase):
    def setUp(self):
        self.conn = sqlite3.connect('testdatabase.sqlite3')

    def test_zonder_tags(self):
        ''' Zoek alle entries waar geen tags aan zijn verbonden. (Resultaat: z) '''
        cursor = self.conn.cursor()
        cursor.execute('''SELECT f.path FROM entries e 
                        JOIN files f ON f.id = e.file_id
                        WHERE e.id NOT IN (
                            SELECT entry_id FROM tagmap
                            )
                        ''')
        results = cursor.fetchall()
        self.assertEqual(
                results,
                [('z',),]
                )

    def test_zoek_a(self):
        """ Zoek alle entries waar de tag 'A' aan verbonden is. (Resultaat: s, u, v, y) """
        cursor = self.conn.cursor()
        # Verkrijg ID van tag 'A'
        cursor.execute('SELECT t.id FROM tags t WHERE t.name = "A"')
        tag_id = cursor.fetchone()[0] 
        # Verkrijg entries met deze tag
        cursor.execute('''SELECT f.path FROM entries e JOIN files f ON f.id = e.file_id
                        WHERE e.id IN (
                            SELECT entry_id FROM tagmap WHERE tag_id = ?
                            )
                        ''', (tag_id,))
        results = cursor.fetchall()
        results.sort()
        self.assertEqual(
                results,
                [('s',), ('u',), ('v',), ('y',)]
                )


if __name__ == '__main__':
    database_generator.maak_database()
    unittest.main()


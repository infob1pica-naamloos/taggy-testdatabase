Deze repo bevat een script waarmee de testdatabase kan worden
gegenereerd. Uiteindelijk is het denk ik ook nuttig om hier een script
te plaatsen dat de verschillende combinaties van dingen er weer uit
kan krijgen, om te kunnen checken of queries juist zijn.

De bestanden die worden aangemaakt hebben een neppad dat bestaat uit een enkele letter van s t/m z.

De verdeling van de tags is als volgt: (bestand z heeft dus geen tags)
* A: s, v, u, y
* B: s, t, v, x
* C: s, t, u, w

De tests die zich in het bestand tests.py bevinden kunnen de volgende dingen opzoeken:
* Alle bestanden zonder tags
* Alle bestanden met de tag 'A'

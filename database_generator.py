# Database generator: maak testdatabase met 3 tags en 1 bestand per combinatie van deze tags
import sqlite3

# Functie zodat het ook in het script kan worden gedraaid
def maak_database():
    # Maak database aan of open bestaande database, importeer schema
    conn = sqlite3.connect('testdatabase.sqlite3')
    with open('taggy_schema.sql', 'r') as f:
        script = f.read()
    conn.executescript(script)
    cur = conn.cursor()

    # Verwijder alle bestaande info in de database om conflicten te voorkomen
    cur.execute('DELETE FROM tags')
    cur.execute('DELETE FROM files')
    cur.execute('DELETE FROM entries')
    cur.execute('DELETE FROM tagmap')

    def char_range(c1, c2):
        """ Geef een iterator die alle tekens van c1 tot en met c2 geeft. Bron: https://stackoverflow.com/a/7001371 """ 
        for c in range(ord(c1), ord(c2)+1):
            yield chr(c)

    # Maak bestanden aan in files
    for file in char_range('s','z'):
        cur.execute('INSERT INTO files (path, broken) VALUES (?, ?)',
                (file, 0))

    # Maak tags aan in table tags
    for tag in char_range('A','C'):
        cur.execute('INSERT INTO tags (name, parent, is_alias) VALUES (?, NULL, 0)',
                (tag,))

    # Maak de entries aan bij de files
    for file in char_range('s', 'z'):
        cur.execute('INSERT INTO entries (file_id) SELECT id FROM files WHERE path = ?',
                (file,))

    # Lees de entries weer in uit de database met hun ID zodat de koppeling sneller kan
    entries = {}
    cur.execute('SELECT files.path,entries.id FROM entries JOIN files ON entries.file_id = files.id')
    for result in cur.fetchall():
        entries[result[0]] = result[1]

    # Lees de tags weer in met hun ID zodat het koppelen sneller kan
    tags = {}
    cur.execute('SELECT tags.name,tags.id FROM tags')
    for result in cur.fetchall():
        tags[result[0]] = result[1]

    # Tuples van bestand naar tag, bestanden en tags moeten bestaan.
    connections = [
            ('y', 'A'),
            ('u', 'A'),
            ('s', 'A'),
            ('v', 'A'),
            ('v', 'B'),
            ('s', 'B'),
            ('t', 'B'),
            ('x', 'B'),
            ('w', 'C'),
            ('u', 'C'),
            ('s', 'C'),
            ('t', 'C')
            ]

    # Maak koppelingen uit connections aan
    for link in connections:
        cur.execute('INSERT INTO tagmap (entry_id, tag_id) VALUES (?, ?)',
                (entries[link[0]], tags[link[1]]))

    # Schrijf naar database
    conn.commit()

if __name__ == '__main__':
    maak_database()
    print('Klaar!')
